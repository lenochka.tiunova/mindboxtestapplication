﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibMath;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestTriangle
    {
        [TestMethod]
        public void TestMethod()
        {
            Triangle areaTriangle = new Triangle(5, 4, 3);
            Assert.AreEqual(6, areaTriangle.CalculateArea());
            areaTriangle = new Triangle(3, 6, 8);
            Assert.AreEqual(7.64, areaTriangle.CalculateArea());
            areaTriangle = new Triangle(5, 7);
            Assert.AreEqual(17.5, areaTriangle.CalculateArea());
        }
    }
}
