﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibMath;


namespace UnitTestProject
{
    [TestClass]
    public class UnitTestCircle
    {
        [TestMethod]
        public void TestMethod()
        {
            Circle areaCircle = new Circle(5, false);
            Assert.AreEqual(19.63, areaCircle.CalculateArea());
            areaCircle = new Circle(2.5);
            Assert.AreEqual(19.63, areaCircle.CalculateArea());
            areaCircle = new Circle(3, true);
            Assert.AreNotEqual(28.26, areaCircle.CalculateArea());
        }
    }
}
