﻿using System;
using LibMath;

namespace CalculateArea
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Triangle triangle = new Triangle(10, 6, 3);
                triangle.PrintArea(nameof(triangle));
            }
            catch (ArithmeticException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Triangle triangle2 = new Triangle(8, 6);
            triangle2.PrintArea(nameof(triangle2));
            Triangle triangle3 = new Triangle(5, 4, 3);
            triangle3.PrintArea(nameof(triangle3));
            Circle circle = new Circle(5);
            circle.PrintArea(nameof(circle));
            circle = new Circle(8,false);
            circle.PrintArea(nameof(circle));
            Console.ReadKey();
        }
    }
}
