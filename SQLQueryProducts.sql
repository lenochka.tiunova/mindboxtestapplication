SELECT ProductTable.ProductName AS 'Продукт', CategoryTable.CategoryName AS 'Категория' FROM ProductTable
LEFT JOIN ProductCategoryTable
ON 
ProductTable.ProductId = ProductCategoryTable.ProductId
LEFT JOIN CategoryTable
ON
CategoryTable.CategoryId = ProductCategoryTable.CategoryId