USE [master]
GO
/****** Object:  Database [dbProducts]    Script Date: 06.05.2021 14:30:39 ******/
CREATE DATABASE [dbProducts]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbProducts', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbProducts.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbProducts_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbProducts_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [dbProducts] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbProducts].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbProducts] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbProducts] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbProducts] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbProducts] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbProducts] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbProducts] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbProducts] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbProducts] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbProducts] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbProducts] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbProducts] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbProducts] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbProducts] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbProducts] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbProducts] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbProducts] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbProducts] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbProducts] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbProducts] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbProducts] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbProducts] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbProducts] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbProducts] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbProducts] SET  MULTI_USER 
GO
ALTER DATABASE [dbProducts] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbProducts] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbProducts] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbProducts] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbProducts] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbProducts] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [dbProducts] SET QUERY_STORE = OFF
GO
USE [dbProducts]
GO
/****** Object:  Table [dbo].[CategoryTable]    Script Date: 06.05.2021 14:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryTable](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CategoryTable] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCategoryTable]    Script Date: 06.05.2021 14:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategoryTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_ProductCategoryTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductTable]    Script Date: 06.05.2021 14:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductTable](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ProductTable] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CategoryTable] ON 

INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (1, N'Хлебобулочные изделия')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (2, N'Плоды и овощи')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (3, N'Кондитерские изделия')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (4, N'Мясные и колбасные изделия')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (5, N'Рыба и рыбные изделия')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (6, N'Яичные изделия')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (7, N'Пищевые жиры')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (8, N'Безалкогольные напитки')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (9, N'Детское питание')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (10, N'Зерно-мучная продукция')
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName]) VALUES (11, N'Молочно-масляная продукция')
SET IDENTITY_INSERT [dbo].[CategoryTable] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductCategoryTable] ON 

INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (1, 1, 1)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (2, 5, 2)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (3, 9, 2)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (4, 12, 3)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (5, 2, 11)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (6, 3, 11)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (7, 6, 6)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (8, 8, 7)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (9, 8, 11)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (10, 4, 4)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (11, 7, 8)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (12, 10, 5)
INSERT [dbo].[ProductCategoryTable] ([Id], [ProductId], [CategoryId]) VALUES (13, 11, 9)
SET IDENTITY_INSERT [dbo].[ProductCategoryTable] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductTable] ON 

INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (1, N'Хлеб')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (2, N'Молоко')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (3, N'Сыр')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (4, N'Колбаса молочная')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (5, N'Картофель')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (6, N'Яйца')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (7, N'Кока-кола')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (8, N'Сливочное масло')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (9, N'Морковь')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (10, N'Карась')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (11, N'Агуша')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (12, N'Конфеты "Ромашка"')
INSERT [dbo].[ProductTable] ([ProductId], [ProductName]) VALUES (13, N'Соль')
SET IDENTITY_INSERT [dbo].[ProductTable] OFF
GO
ALTER TABLE [dbo].[ProductCategoryTable]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategoryTable_CategoryTable] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[CategoryTable] ([CategoryId])
GO
ALTER TABLE [dbo].[ProductCategoryTable] CHECK CONSTRAINT [FK_ProductCategoryTable_CategoryTable]
GO
ALTER TABLE [dbo].[ProductCategoryTable]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategoryTable_ProductTable] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ProductTable] ([ProductId])
GO
ALTER TABLE [dbo].[ProductCategoryTable] CHECK CONSTRAINT [FK_ProductCategoryTable_ProductTable]
GO
USE [master]
GO
ALTER DATABASE [dbProducts] SET  READ_WRITE 
GO
