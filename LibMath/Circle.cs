﻿using System;

namespace LibMath
{
    /// <summary>
    /// Implementation of the base class circle.
    /// </summary>
    public class Circle : Figure
    {
        /// <summary>
        /// Radius or diameter of a circle.
        /// </summary>
        private double lenght;

        /// <summary>
        /// Boolean value to determine if the entered value is a radius or a diameter.
        /// </summary>
        private bool radius;

        /// <summary>
        /// Implementation of the constructor of the Circle class.
        /// </summary>
        /// <param name="a">Radius or diameter of a circle.</param>
        /// <param name="r">Boolean value to determine if the entered value is a radius or a diameter.</param>
        public Circle(double a, bool r = true)
        {
            lenght = a;
            radius = r;
        }

        /// <see cref="Figure"/>
        public override double CalculateArea()
        {
            if (radius)
            {
                return Math.Round(Math.PI * Math.Pow(lenght,2), 2);
            }
            else
            {
                return Math.Round(Math.PI * Math.Pow(lenght/2, 2), 2);
            }
        }
    }
}
