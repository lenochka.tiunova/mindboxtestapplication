﻿using System;

namespace LibMath
{
    /// <summary>
    /// Implementation of the base class figure.
    /// </summary>
    abstract public class Figure
    {
        /// <summary>
        /// Method for calculating the area of a figure.
        /// </summary>
        /// <returns>Return area of a figure.</returns>
        public abstract double CalculateArea();

        /// <summary>
        /// Displaying the area of a figure on the screen.
        /// </summary>
        public void PrintArea(string typeFigure)
        {
            Console.WriteLine("Area of " + typeFigure + " = " + CalculateArea());
        }
    }
}
