﻿using System;

namespace LibMath
{
    /// <summary>
    /// Implementation of the base class triangle.
    /// </summary>
    public class Triangle : Figure
    {
        /// <summary>
        /// Side 1.
        /// </summary>
        private double side1;

        /// <summary>
        /// Side 2.
        /// </summary>
        private double side2;

        /// <summary>
        /// Side 3.
        /// </summary>
        private double side3;

        /// <summary>
        /// Variable to define the formula for calculating the triangle.
        /// </summary>
        private string calculateType;

        /// <summary>
        /// Implementation of the constructor of the Triangle class. Create a triangle on three sides.
        /// </summary>
        /// <param name="a">Side 1.</param>
        /// <param name="b">Side 2.</param>
        /// <param name="c">Side 3.</param>
        public Triangle(double a, double b, double c)
        {
            //Testing for the existence of a triangle.
            if (IsTriangleExist(a, b, c))
            {
                side1 = a;
                side2 = b;
                side3 = c;
                calculateType = "threeSide";
            }
            else
            {
                throw new ArithmeticException("Triangle with given sides could not be created");
            }
        }

        /// <summary>
        /// Implementation of the constructor of the Triangle class. Create a triangle in height and base.
        /// </summary>
        /// <param name="a">Base.</param>
        /// <param name="h">Height.</param>
        public Triangle(double a, double h)
        {
            side1 = a;
            side2 = h;
            calculateType = "sideAndHight";
        }

        /// <summary>
        /// Helper method for checking if triangle can be created.
        /// </summary>
        /// <param name="a">Side 1.</param>
        /// <param name="b">Side 2.</param>
        /// <param name="c">Side 3.</param>
        /// <returns>Return true if triangle exists.</returns>
        private bool IsTriangleExist(double a, double b, double c)
        {
            if (a < b + c && b < a + c && c < a + b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  Helper method for checking if triangle is rectangular.
        /// </summary>
        /// <param name="a">Side 1.</param>
        /// <param name="b">Side 2.</param>
        /// <param name="c">Side 3.</param>
        /// <returns>Return true if triangle is rectangular.</returns>
        public bool IsTriangleRectangular(double a, double b, double c)
        {
            if (a*a == b * b + c * c || b * b == a * a + c * c || c * c == a * a + b * b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  Helper method for getting legs of right triangle.
        /// </summary>
        /// <param name="a">Side 1.</param>
        /// <param name="b">Side 2.</param>
        /// <param name="c">Side 3.</param>
        /// <returns>Return legs of right triangle.</returns>
        private double[] GetLegsOfTriangle(double a, double b, double c)
        {
            if (a * a == b * b + c * c)
            {
                return new double[2] {b, c};
            }
            else if (b * b == a * a + c * c)
            {
                return new double[2] {a, c};
            }
            else
            {
                return new double[2] {a, b};
            }
        }

        /// <see cref="Figure.CalculateArea"/>
        public override double CalculateArea()
        {
            switch (calculateType)
            {
                case "threeSide":
                    return CalculateAreaByThreeSide();

                case "sideAndHight":
                    return CalculateAreaBySideAndHeight();

                default:
                    return 0;
            }
        }

        /// <summary>
        /// Method for calculating the area on three sides.
        /// </summary>
        /// <returns>Return area value.</returns>
        public double CalculateAreaByThreeSide()
        {
            if (IsTriangleRectangular(side1, side2, side3))
            {
                double[] legs = GetLegsOfTriangle(side1, side2, side3);
                return Math.Round((legs[0] * legs[1] / 2), 2);
            }
            else
            {
                double sPerimeter = (side1 + side2 + side3) / 2;
                return Math.Round(Math.Sqrt(sPerimeter * (sPerimeter - side1) * (sPerimeter - side2) * (sPerimeter - side3)), 2);
            }
        }

        /// <summary>
        /// Method for calculating area by base and height.
        /// </summary>
        /// <returns>Return area value.</returns>
        public double CalculateAreaBySideAndHeight()
        {
            return Math.Round(0.5 * side1 * side2, 2);
        }
    }
}
